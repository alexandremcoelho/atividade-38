import { Component } from "react";

export class Character extends Component {
  render() {
    console.log(this.props.list);
    return (
      <div class="personagens">
        <img src={this.props.list.image}></img>
        <div>{this.props.list.name}</div>
        <div>{this.props.list.house}</div>
        <div>{this.props.list.gender}</div>
      </div>
    );
  }
}
