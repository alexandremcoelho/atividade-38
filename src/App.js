import { Component } from "react";
import "./App.css";
import { Character } from "./components/character";

class App extends Component {
  state = {
    characters: [],
    newCharacters: [],
    updated: false,
    clicado: false,
  };

  componentDidMount() {
    const { characters } = this.state;
    fetch("https://hp-api.herokuapp.com/api/characters/students")
      .then((response) => response.json())
      .then((response) => {
        this.setState({ characters: response });
      });
    let newArray = [];
  }
  componentDidUpdate() {
    if (this.state.updated === false) {
      const { newCharacters } = this.state;
      const { updated } = this.state;
      let newArray = [0, 0, 0];
      let boll = true;
      for (let i = 0; i < 3; i++) {
        while (boll) {
          newArray[0] = Math.floor(Math.random() * (11 - 0) + 0);
          newArray[1] = Math.floor(Math.random() * (11 - 0) + 0);
          newArray[2] = Math.floor(Math.random() * (11 - 0) + 0);
          if (
            newArray[0] !== newArray[1] &&
            newArray[1] !== newArray[2] &&
            newArray[0] !== newArray[2]
          ) {
            if (
              this.state.characters[newArray[0]].house !==
                this.state.characters[newArray[1]].house &&
              this.state.characters[newArray[1]].house !==
                this.state.characters[newArray[2]].house &&
              this.state.characters[newArray[0]].house !==
                this.state.characters[newArray[2]].house
            ) {
              boll = false;
            }
          }
        }
        newArray.push(this.state.characters[newArray[i]]);
      }
      this.setState({ newCharacters: [...newArray] });
      this.setState({ updated: true });
    }
    console.log(this.state.newCharacters);
  }
  handleClick = () => {
    this.setState((state) => ({
      clicado: !state.clicado,
      updated: false,
    }));
  };

  render() {
    //console.log(this.state.newCharacters);

    return (
      <>
        <button onClick={this.handleClick} id="botao">
          JOGAR
        </button>
        <>
          {this.state.clicado && (
            <div id="container">
              {this.state.newCharacters.map((item, index) => (
                <Character key={index} list={item} />
              ))}
            </div>
          )}
        </>
      </>
    );
  }
}

export default App;
